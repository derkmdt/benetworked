﻿package com.general {
	import flash.display.Sprite;
	import flash.events.Event;
	public class Smokeparticle extends Sprite {
		private var particles:Array = new Array();
		private var particle:Object;
		private var counter:Number =0;
		private var bursting:Boolean = false; 
		public function Smokeparticle():void {
			addEventListener(Event.ENTER_FRAME, enterFrame); 
			function enterFrame(e:Event):void {
				counter++; 
				updateParticles();
				if(counter%2==0) addParticle(1); 
			}
		}
		private function addParticle(particlecount:int):void	{
			for(var i:int=0; i< particlecount; i++)	{
				// make a new particle
				particle = new Particle(Smoke, this, 450, 300)
					
				// give it a random velocity 
				particle.setVel(randRange(-1,1),0);
				
				// give it a random size
				particle.clip.scaleX = particle.clip.scaleY = randRange(0.2,0.8);
			
				// make it transparent
				particle.clip.alpha = 0.3;
				
				particle.drag = 0.98;
				particle.fade = 0.97;
				particle.shrink = 1.05;
				particle.gravity = -0.5;
			
				// and add it to the particle array... 
				particles.push(particle);
			}
		}
		private function updateParticles():void	{
			var particle:Particle;
			// if there are more than 40 particles delete the first one in the array... 
			while(particles.length>50) {
				particle = particles.shift();
				particle.destroy(); 
			}
			// go through the array of particles... 
			for(var i:int = 0;i<particles.length;i++) {
				//... and update each one
				particle = particles[i];
				particle.update();
			}
		}
		private function randRange(min:Number, max:Number) {
			var randomNum:Number =Math.random() * (max - min) + min;
			return randomNum;
		}
	}
}
