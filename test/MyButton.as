﻿package {
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;
 
	public class MyButton extends MovieClip {

		public function MyButton(labelName:String) {
			label_txt.text = labelName; // Grab that parameter and label myself!
 
			addEventListener(MouseEvent.CLICK, clickMyButton);
			addEventListener(MouseEvent.MOUSE_OVER, rollOverMyButton);
			addEventListener(MouseEvent.MOUSE_OUT, rollOutMyButton);
 
		}
 
		public function clickMyButton(e:Event = null):void
		{
			playGame();
		}
 
		public function rollOverMyButton(e:Event = null):void
		{
			background.gotoAndStop("highlight");
		}
 
		public function rollOutMyButton(e:Event = null):void
		{
			background.gotoAndStop("unhighlight");
		}
 
		public function playGame():void
		{
			// Do some stuff here
		}
 
	}
}