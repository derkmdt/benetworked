## What is BeNetworked?
BeNetworked is a first version of a casual flash game i'm building. It is based on the example that emanueleferonato.com posted on his blog.

## Todo
* adding a Game Over and the possibility of a restart
* adding a real timer, where users are bound to a time limit
* adding points visually when blocks are removed
* adding other feedback then current explosions
* making a nicer hint feature
* posting highscores to a server
* adding your name to a highscore
* optimizing the game. Now i'm not looking at file sizes or cleaning up code
* commenting on code

## Future possibilities
* Adding levels like the real Bejeweled has
* Adding Bonus points. Mega or Super...